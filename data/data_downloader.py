"""
Run this script to download data from the NYC TLC website.
"""
import boot
import os
import logging
import fire

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options

# Since Selenium requires a binary for the browser, it is cumbersome to:
# Manually download the binary -> unzip it -> add it to PATH env variable, so the Selenium can find it
# And repeat this process when the binary version updates
# Webdriver Manager is used to simplify the management of binary drivers for browsers
from webdriver_manager.chrome import ChromeDriverManager

from src.logging_utils import init_logger


class YellowTripDataDownloader:
    """

    """
    def __init__(self, year: int = 2020):
        """
        :param year: Year of the dataset to download
        """
        self.logger = logging.getLogger(__class__.__name__)
        init_logger(self.logger)

        self.dataset_path = f"{os.path.dirname(__file__)}/datasets"
        self.dataset_name = "yellow_tripdata"
        self.url = "https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page"
        self.year = year

        service = Service(executable_path=ChromeDriverManager().install())
        options = Options()
        options.add_argument("--headless")

        self.driver = webdriver.Chrome(service=service, options=options)

    def initialize_dataset_folder(self) -> None:
        """
        Check if folder to download data to already exists, if not then create the folder
        :return: None
        """
        if not os.path.exists(self.dataset_path):
            os.makedirs(self.dataset_path)
            self.logger.info(f"Created folder {self.dataset_path}")
        self.logger.info(f"Path {self.dataset_path} exists")

    def __call__(self) -> None:
        """
        Run the data downloader. Get all elements on the website that contain links.
        The website does not encode filenames, so a simple check on dataset name and year in the filename
        helps find the data files easily.
        :return: None
        """
        self.initialize_dataset_folder()

        try:
            self.driver.get(self.url)
            links = self.driver.find_elements(by=By.XPATH, value=f"//a[@href]")

            for link in links:
                if f"{self.dataset_name}_{self.year}" in link.get_attribute("href"):
                    self.logger.info(f"Downloading {link.get_attribute('href')} ...")
                    dataset_link = link.get_attribute("href")
                    filename = dataset_link.split("/")[-1]
                    r = requests.get(dataset_link)

                    with open(f"{self.dataset_path}/{filename}", "wb") as f:
                        f.write(r.content)
                        self.logger.info(f"Saved dataset to {self.dataset_path}/{filename}")
        except Exception as e:
            self.logger.error(f"Download could not be completed. {repr(e)}")
            raise
        else:
            self.logger.info("Download complete!")
            self.driver.quit()


if __name__ == "__main__":
    fire.Fire(YellowTripDataDownloader)
