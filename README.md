# Tip Predictor

Model the tips that a taxi driver would receive for a ride.

# Business Case
Our company has made a mobile app for taxi drivers. One of the products on the mobile app is to estimate the 
tip amount the taxi driver would receive for their ride. 

__Hypothesis__ :
Showing a tip amount to the taxi driver will encourage them to accept more rides.

# Assumptions
The following assumptions were made while designing the app for this assignment:
* This app is a microservice that communicates directly with the mobile app
* The mobile app is capable to provide all features that are necessary for making the prediction
* Showing the tip information on the app directly leads to a decision on accepting rides or not.

# Overview of Project
The contents of this project are as follows:

```
tips
├── analysis            -> Contains scripts for model evaluation analysis
├── app                 -> Contains scripts for launching a server using gRPC API.
├                       -> It also simulates a client for testing purposes.
├── data                -> Contains script to download the data from the website
│   └── datasets        -> Downloads data here
├── src                 -> Contains logic for running model training and serving pipeline 
    ├── data_processing -> Module for feature processing
    ├── evalation       -> Module for model performance evaluation
    └── models          -> Module for defining models
```

# Setup 
Follow the steps to set up this project:
1. Go into the project directory: `cd tips`
2. Create your virtual environment using `pyenv virtualenv`. Install [pyenv virtualenv](https://github.com/pyenv/pyenv-virtualenv) if you don't have it. 
   ```shell
   pyenv virtualenv 3.7.4 <venv-name>
   pyenv activate <venv-name>
   ```
   Note: This app was built using python 3.7.4
4. To install development requirements
   ```shell
   pip install -e .
   ```
   
# Instructions

1. Go into the project directory: `cd tips`
2. To download the dataset, run the command:
   ```shell
   python ./data/data_downloader.py --year 2020
   ```
   Arguments:
   * `year`: Dataset year 
   
   This script downloads the dataset of all the months for the specified year.
3. To train the model, run the command:
   ```shell
   tip_predictor_train --model_name "ElasticNetModel" --processor_name "FeatureProcessor" --year 2020 --month 1 --verbosity 1                           
   ```
   This command will save the Feature Processor and Model objects trained on the training dataset as pickle files to
   `./trained_models/FeatureProcessorv1.pkl` and `./trained_models/ElasticNetModel.pkl`
   Arguments:
   * `model_name`: Name of the model
   * `processor_name`: Name of the Feature Processor
   * `year`: Dataset year
   * `month`: Dataset month
   * `verboslity`: Logging verbosity

4. To serve predictions online, run the following commands:
   1. Start the server, once it starts receiving requests from clients it will serve back predictions.
      ```shell
      tip_predictor_serve_online
      ```
      This script invokes the `serve_online` method from `src/runner.py` to serve predictions online.
   
   2. * Start the client,
      ```shell
      python ./app/client.py
      ```
      This script generates random data and sends it to the server over `localhost:9999`.
      You can spin up concurrent clients in new terminals to increase load on the server.
   
      Alternatively,
   
      * To load test the application, run the following command:
      ```shell
      locust -f ./app/locust_load_test.py    
      ```
      This script simulates concurrent Users as GRPC Clients and measure observability metrics such as `Requests per Second`
      and `Response time`. It is useful to measure how the app performs with different user behaviour and locust does so 
      by swarming the system with concurrent users. Analysis on such performance metrics can help at setting 
      thresholds for sending alerts, automate scaling during load bursts or helps deep dive into reducing latency of 
      the service. Open `localhost:8089` on your browser to view the charts and statistics. 
      * Note: Keep the server running at the same time using the above command.
      * Example: 
      ![App Performance](./analysis/app_performance.png)
5. To test the model on a dataset, run the command:
      ```shell
      tip_predictor_serve_test --model_name "ElasticNetModel" --processor_name "FeatureProcessor" --path "./data/processed_datasets/yellow_tripdata_2020-01_test_features.parquet" --verbosity 1                               
      ```
      Arguments:
      * `model_name`: Name of the model
      * `processor_name`: Name of the Feature Processor
      * `path`: Path to dataset features
      * `verboslity`: Logging verbosity
   
      This script saves the predictions to a file in `tips/data/datasets/yellow_tripdata_2020-01_test_predictions.parquet`
6. Check-out the analysis performed in the notebook. `./analysis/analysis.ipynb`
The notebook loads the test features set, test target and predictions generated in step `3` to compute model 
performance metrics on this set on different aggregate levels of interest. Such analysis can also be performed in online 
settings when we save the tip amounts the taxi driver actually received.

# Prediction Service App

### GRPC
Google Remote Procedure Call is a client-server model for communication based on the RPC protocol. 
In a RPC API, a client requests a message that is translated by the RPC protocol and sent to the main server.
On receiving the request, the server sends back a response to the client. 
RPC allows the client to request a function in a specific format and receives response in the defined format.

#### Why GRPC for this assignment?
* RPC is based on HTTP 2.0 which is faster than HTTP1.1. RPC abstracts away the HTTP requests and handles the mapping 
of RPC requests to HTTP requests. So, as a developer I do not need to create different types of HTTP requests like 
GET or POST. Moreover, it provides in-built code generation, so I am only concerned with calling the required 
methods in 
* It expresses the API model in an IDL (Interface Description Language) which makes it very straightforward 
to develop and determine the remote procedures. It uses Protocol Buffers to describe the service interface and 
payload message structure.
* Although, for this application a Unary RPC channel was made which is similar to a REST-API. 
RPC offers the message to be collected in a specific format (defined in the proto file) and serialized directly to bytes 
instead of a JSON/XML file which will later have to be parsed to extract information in the correct data-types. 
This reduces overhead in serialization and deserialization because the payload follows a strict API contract.
* Due to its light-weight message format, GRPC is well suited for network constrained communication such as with mobile 
apps. It also heps save bandwidth on the client side and saves CPU usage and battery life. 
This assignment is designed to serve a mobile app.

### API Description

#### Prediction Service
The Prediction Service accepts input features that are required by the model and serves predictions as responses.
Predictions are the expected tip amount that would be received by the taxi driver.

#### Payload
The payload received from the client for every unique `driver_id` and `ride_id` is defined as: 
```proto3
message Features {
    fixed32 driver_id = 1;
    fixed32 ride_id = 2;
    google.protobuf.Timestamp tpep_pickup_datetime = 4;
    google.protobuf.Timestamp tpep_dropoff_datetime = 5;
    fixed32 passenger_count = 6;
    float trip_distance = 7;
    fixed32 ratecodeid = 8;
    fixed32 payment_type = 11;
    float fare_amount = 12;
}
```
And the server serves the client back with the following payload for that specific `driver_id` and `ride_id`
```proto3
message Prediction {
    fixed32 driver_id = 1;
    fixed32 ride_id = 2;
    float value = 3;
}
```
The client communicates with the server using a service on `localhost:9999`

# Modeling Details
Trained 2 different models, ElasticNet and Random Forest. 
Selected ElasticNet Model based on the following reasons:
1. ElasticNet
   1. Simple and interpretable model.
   2. Lightweight model as a result had faster training and serving times. 
   3. Serving predictions was 10 times faster than with Random Forest model.
   
   Note: To train good linear models, data should exhibit characteristics that fall under the assumptions made by a Linear Model.
   However, this was not explored due to time constraints.
2. Random Forest
   1. To achieve good performance with random forest needed to train deep trees in order to have a low bias. 
   But deep trees required a lot of computational resources to evaluate, due to traversing every tree in the 
   forest and evaluating splitting conditions on every level. Thus training and serving times were long.
   2. Less Interpretability. 
   3. The hyper parameters were not explored to the fullest thus the model trained was not performing well
   4. Random Forests fails when there are rare outcomes or rare predictors, as the algorithm is based on bootstrap sampling.

# Online Monitoring Metrics 
This section describes how would we validate whether the role out of this feature actually helps achieve the business 
goal stated in our hypothesis. We could measure the following metrics to validate our hypothesis 
(in order of importance):
1. Based on assumption 3, measure the `Ride Acceptance Rate` which is defined as ratio of the number of rides accepted 
to the number of rides shown to the driver on the mobile app. An A/B Test can be conducted by rolling out this feature 
to a small random set of drivers who would see this feature (Variation Group) whereas the rest of the drivers who do not 
see this feature (Control Group). 
2. A change in ride acceptance rate will also affect the `Transaction Margins` the company makes. If showing the tip 
prediction has a positive impact on `Ride Accpetance Rate`, then this will also show increase in monetary gains 
due to increased commissions.
3. Depending on the user experience flow and what stage of this flow is the tip prediction shown to the user, 
we can also expect the `Requests per Second` that the server receives as a sign of usage. Consider a scenario, where 
the tip predictions are shown after some features are taken as inputs from the user itself. This would encourage them 
to try out different combinations of inputs fed to the app and as a result receive requests to make a decision to accept 
the ride or not. Moreover, this metric can also be used to take actions when alerts are triggered for bursts in number 
of requests. This can help to make deployment strategies like autoscaling pods (if deployed using kubernetes).
4. If tip predictions are close enough to the actual tip received then this would make the driver satisfied. We can 
measure the actual tip received through the mobile app and save this data locally or in tables to query later for analysis. 
Calculate the `Mean Absolute Error` on specific aggregate levels as shown in the analysis script but this time on live data. 
This will provide indications on model drift and hence help make decisions on possible model performance improvements. 
Driver satisfaction can cause some increase in `NPS` i.e. the Net Promotor Score over upcoming quarters.

We can track these metrics based on the user interaction with this feature on the app. The data for this analysis can 
also be collected in a similar way using the GRPC protocol and stored into tables for further analysis.
