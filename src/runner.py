"""
Wrapper methods to run training and serving pipelines
"""

import pandas as pd

from src.training_pipeline import TrainingPipeline
from src.serving_pipeline import ServingPipeline
from src.data_processing.utils import save_data


def train(model_name: str,
          processor_name: str,
          year: int,
          month: int,
          verbosity: int = 0) -> None:
    """
    Wrapper method to run Training Pipeline
    :param model_name: Name of the model
    :param processor_name: Name of the Feature Processor
    :param year: Year of training Dataset
    :param month: Month of training Dataset
    :param verbosity: Logging Verbosity (Default: INFO)
    :return:
    """
    train_pipe = TrainingPipeline(
        model_name, processor_name, year, month, verbosity
    )
    train_pipe.run()


def serve_test(path: str,
               model_name: str,
               processor_name: str,
               verbosity: int = 0) -> pd.Series:
    """
    Wrapper method to serve predictions on unseen data set
    :param path: Test Input features from file
    :param model_name: Name of the model
    :param processor_name: Name of the Feature Processor
    :param verbosity: Logging Verbosity
    :return: prediction value
    """
    serve_pipe = ServingPipeline(
        model_name, processor_name, verbosity, mode=1
    )
    predictions = serve_pipe.run(path)
    output_path = path.split("_test_")[0] + "_test_predictions.csv"
    save_data(predictions, f"{output_path}")
    return predictions
