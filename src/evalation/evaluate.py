import logging
from typing import List, Dict

import numpy as np
import pandas as pd

from sklearn.metrics import mean_absolute_error

from src.constants import TARGET, FEATURE_ATTRIBUTES
from src.logging_utils import init_logger


class Evaluator:
    """
    Evaluates Model performance on datasets
    """
    def __init__(self,
                 features: pd.DataFrame,
                 targets: pd.Series,
                 predictions: pd.Series):
        """
        Initialize Evaluator
        :param features: Features Dataframe
        :param targets: Target Series for the same index as features
        :param predictions: Predictions Series for the same index as features
        """
        self._logger = logging.getLogger(__class__.__name__)
        init_logger(self._logger)

        self._dimensions = features.columns.values
        self._data = pd.concat([features, targets, predictions], axis=1)

    def evaluate_over_entire_dataset(self) -> Dict:
        """
        Calculate metrics over entire dataset
        :return: Mean Absolute Error & Bias
        """
        metrics = {
            "MAE": mean_absolute_error(self._data[TARGET], self._data["predictions"]),
            "Bias": np.mean(self._data[TARGET] - self._data["predictions"])
        }
        return metrics

    def evaluate_over_dimension(self, dimension: str, bins: List) -> Dict:
        """
        Calculate metrics aggregated over a dimension
        :param dimension: Name of the dimension to calculate aggregate metrics over
        :param bins: If dimension is Numerical, specify bins to cut into
        :return: Dictionary of Mean Absolute Error & Bias
                 over groups in the dimension
        """
        if dimension not in self._dimensions:
            self._logger.error(f"Dimension {dimension} not found in feature dimensions.")
            raise ValueError(f"Dimension {dimension} not found in features dimensions.")
        else:
            metrics_over_groups = {}

            if FEATURE_ATTRIBUTES[dimension]["type"] == "Numerical":
                self._data[f"{dimension}_bin"] = pd.cut(self._data[dimension], bins)

                grouped_data = self._data.copy().groupby(by=f"{dimension}_bin")
            elif FEATURE_ATTRIBUTES[dimension]["type"] == "Categorical":
                grouped_data = self._data.copy().groupby(by=dimension)
            else:
                self._logger.error("No feature to aggregate over Datetime dimensions")
                raise NotImplementedError

            for group_name, data in grouped_data:
                metrics = {
                    "MAE": mean_absolute_error(data[TARGET], data["predictions"]),
                    "Bias": np.mean(data[TARGET] - data["predictions"])
                }
                metrics_over_groups[f"{dimension}_{group_name}"] = metrics

        return metrics_over_groups
