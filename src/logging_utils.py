import sys
import logging


def init_logger(logger, verbose: int = 0) -> None:
    """
    Initialize and configure logging
    Args:
        logger: Logger object
        verbose: Verbosity level. 0: INFO, 1: DEBUG
    """
    if verbose >= 1:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(
        stream=sys.stdout,
        format="%(asctime)s | %(levelname)-8s | %(process)d | %(name)s:%("
               "funcName)s:%(lineno)d - %(message)s",
        level=level,
    )

    logger.setLevel(level)

