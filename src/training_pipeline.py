import os
import logging

from src.data_processing.factory import get_feature_processor
from src.data_processing.utils import (
    read_data,
    split_features_and_targets,
    split_train_test,
    save_data
)
from src.models.factory import get_model
from src.constants import (
    DATASETS_PATH,
    PROCESSED_DATASET_PATH,
    DATASET_NAME,
    MODELS_PATH,
    MODEL_HYPERPARAMETERS
)

from src.logging_utils import init_logger


class TrainingPipeline:
    """
    Pipeline used to train model
    1. Prepares data
    2. Process features
    3. Trains the model
    """
    def __init__(self,
                 model_name: str,
                 processor_name: str,
                 year: int,
                 month: int,
                 verbosity: int = 0):
        """
        Initialize pipeline
        :param model_name: Name of the model used for training
        :param processor_name: Name of the feature processor
        :param year: Dataset year
        :param month: Dataset month
        :param verbosity: Logging verbosity
        """
        self._logger = logging.getLogger(__class__.__name__)
        init_logger(self._logger, verbosity)

        self.year = year
        self.month = f"0{month}" if month < 10 else f"{month}"
        self.processor_name = processor_name
        self.model_name = model_name

        self.data_path = f"{DATASETS_PATH}{os.sep}{DATASET_NAME}_{self.year}-{self.month}.parquet"

        self.train_features_path = f"{PROCESSED_DATASET_PATH}{os.sep}{DATASET_NAME}_" \
                                   f"{self.year}-{self.month}_train_features.parquet"
        self.train_target_path = f"{PROCESSED_DATASET_PATH}{os.sep}{DATASET_NAME}_" \
                                 f"{self.year}-{self.month}_train_target.csv"
        self.test_features_path = f"{PROCESSED_DATASET_PATH}{os.sep}{DATASET_NAME}_" \
                                  f"{self.year}-{self.month}_test_features.parquet"
        self.test_target_path = f"{PROCESSED_DATASET_PATH}{os.sep}{DATASET_NAME}_" \
                                 f"{self.year}-{self.month}_test_target.csv"

        self.feature_processor = get_feature_processor(
            processor_name=self.processor_name,
            encoder_path=MODELS_PATH,
            verbosity=verbosity)

        self.model = get_model(
            model_name=self.model_name,
            hparams=MODEL_HYPERPARAMETERS[self.model_name],
            verbosity=verbosity)

    def prepare_data(self) -> None:
        """
        Reads and saves data files after splitting into train test sets and features and target
        :return: None
        """
        self._logger.info(f"Loading data from year {self.year}, "
                          f"month {self.month}...")
        data = read_data(self.data_path)
        features, target = split_features_and_targets(data)

        features_train, features_test, \
        target_train, target_test = split_train_test(features, target)

        save_data(features_train, self.train_features_path)
        save_data(features_test, self.test_features_path)
        save_data(target_train, self.train_target_path)
        save_data(target_test, self.test_target_path)

    def process_features(self) -> None:
        """
        Saves processed features to a file and Feature Processor object as pickle
        :return: None
        """
        self._logger.info(f"Processing Features for the model...")
        features = self.feature_processor.extract(method="file",
                                                   path=self.train_features_path)
        transformed_features = self.feature_processor.fit_transform(data=features)

        self.feature_processor.load(
            data=transformed_features,
            output_path=self.train_features_path)

    def train(self) -> None:
        """
        Trains the model and saves model object as pickle
        :return: None
        """
        self._logger.info("Begin Training...")
        X = read_data(self.train_features_path)
        y = read_data(self.train_target_path)

        self.model.fit(X, y)
        self.model.save()

    def run(self) -> None:
        """
        Runs the entire pipeline
        :return: None
        """
        self.prepare_data()
        self.process_features()
        self.train()