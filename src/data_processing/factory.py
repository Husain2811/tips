from src.data_processing.base_processor import BaseDataProcessor
from src.data_processing.data_processor import FeatureProcessor


def get_feature_processor(processor_name: str, encoder_path: str, verbosity: int = 0) -> BaseDataProcessor:
    """
    Return the Feature Processor selected by processor name

    :param processor_name: Name of the processor to use
    :param encoder_path: Path where encoder is saved and loaded from
    :param verbosity: int
    :return: DataProcessor
    :raises NotImplementedError: If not supported yet.
    """
    processors = {
        f"{FeatureProcessor.__name__}": FeatureProcessor(encoder_path, verbosity),
    }

    try:
        return processors[processor_name]
    except KeyError:
        raise ModuleNotFoundError(f"Module for {processor_name} data processor is not implemented yet...")
