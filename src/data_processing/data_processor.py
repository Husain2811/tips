import os
import logging
from typing import Dict
import pickle

import pandas as pd
from sklearn.preprocessing import OneHotEncoder

from src.constants import FEATURE_ATTRIBUTES
from src.logging_utils import init_logger
from src.data_processing.base_processor import BaseDataProcessor
from src.data_processing.utils import read_data, save_data, get_features_from_attribute


class MissingFeatureError(AssertionError):
    pass


class FeatureProcessor(BaseDataProcessor):
    """
    Contains methods for Extract-Transform-Load:
    1. Extract data from file path or raw format depending on method requested
    2. Transform features suitable for model training purpose
    3. Load transformed data into output files
    """
    def __init__(self, save_path: str, verbosity: int = 0):
        """
        Initialize
        :param save_path: Path to save the Feature Processor object as a pickle
        :param verbosity: logging verbosity
        """
        super().__init__()
        self._logger = logging.getLogger(__class__.__name__)
        init_logger(self._logger, verbosity)

        self._feature_attributes = FEATURE_ATTRIBUTES
        self._encoder = OneHotEncoder()
        self._save_path = save_path

    def extract(self, method: str, **kwargs) -> pd.DataFrame:
        """
        Wrapper method for extracting Data from source specified in **kwargs using method
        :param method: Method to extract data
        :param kwargs: kwargs for specific method
        :return: DataFrame
        """
        extract_method = {
            "file": self._extract_from_file,
            "json": self._extract_from_dict,
        }
        try:
            data = extract_method[method](**kwargs)
        except KeyError as e:
            self._logger.error(f"Method not implemented. Error: {repr(e)}")
            raise NotImplementedError
        else:
            return data

    @staticmethod
    def _extract_from_file(path: str):
        """
        Load Data from dataset path
        :param path: Dataset path
        :return: DataFrame
        """
        data = read_data(path)
        return data

    def _extract_from_dict(self, json: Dict):
        """
        Extract Data From Json dict
        :param json: Data in Dictionary
        :return: DataFrame
        """
        feature_columns = get_features_from_attribute(attribute="drop", value=False)
        data = pd.DataFrame(data=[[v for k, v in json.items()]], columns=feature_columns)
        datetime_columns = get_features_from_attribute(attribute="type", value="Datetime")
        data[datetime_columns] = pd.to_datetime(data[datetime_columns].stack(),
                                                format="%Y-%m-%dT%H:%M:%S.%fZ").unstack()
        self._logger.info(f"Loaded {data.shape[0]} row and {data.shape[1]} columns from data dictionary")
        return data

    def fit_transform(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Transform features by extracting datetime features,
        fit and transform one-hot encoding th
        :param data: Input DataFrame
        :return: Transformed Features DataFrame
        """
        try:
            data = data.copy()
            self._validate_data(data)
            features = data.pipe(self._extract_datetime_features) \
                           .pipe(self._fit_transform_one_hot_categorical) \
                           .pipe(self._impute_nans)
            self.save_processor()
        except Exception as e:
            error_message = f"Features could not be transformed. Error: {repr(e)}"
            self._logger.error(error_message)
            raise
        else:
            self._logger.info("Successfully fitted and transformed features")
            self._logger.debug(f"Feature columns: \n {features.columns.values}")
            self._logger.info(f"Data size: {features.shape}")
            return features

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Transform features by extracting datetime features,
        transform using one-hot encoding th
        :param data: Input DataFrame
        :return: Transformed Features DataFrame
        """
        try:
            data = data.copy()
            self._validate_data(data)
            features = data.pipe(self._extract_datetime_features) \
                           .pipe(self._transform_one_hot_categorical) \
                           .pipe(self._impute_nans)
        except Exception as e:
            error_message = f"Features could not be transformed. Error: {repr(e)}"
            self._logger.error(error_message)
            raise
        else:
            self._logger.info("Successfully transformed features")
            self._logger.debug(f"Feature columns: \n {features.columns.values}")
            self._logger.info(f"Data size: {features.shape}")

            return features

    def _validate_data(self, data: pd.DataFrame):
        """
        Check if all columns specified in attributes match input data columns
        :param data: Input Data
        :return: None
        :raises MissingFeatureError
        """
        feature_columns = get_features_from_attribute(attribute="drop", value=False)
        diff = set(feature_columns) - set(data.columns.values)
        if len(diff) > 0:
            raise MissingFeatureError(f"Cannot proceed. The following features are missing: {[f for f in diff]}")
        else:
            self._logger.info("Validated input data")

    def _extract_datetime_features(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Extract features from datetime columns
        :param data: Input Data
        :return: Data after extracting features from datetime columns
        """
        data.loc[:, "ride_duration"] = self._get_ride_duration_mins(data)
        self._feature_attributes["ride_duration"] = {"type": "Numerical"}
        self._logger.debug("Feature added: ride_duration")

        data.loc[:, "pickup_hour_of_day"] = self._get_pickup_hour_of_day(data)
        self._feature_attributes["pickup_hour_of_day"] = {"type": "Categorical"}
        self._logger.debug("Feature added: pickup_hour_of_day")

        datetime_features = get_features_from_attribute(attribute="type",
                                                        value="Datetime")
        data = data.drop(columns=datetime_features)
        self._logger.debug(f"Datetime Features {datetime_features} dropped")
        return data

    @staticmethod
    def _get_pickup_hour_of_day(data: pd.DataFrame) -> pd.Series:
        """
        Extract hour of day from pickup datetime
        :param data: Input Data
        :return: Hour of day (Data Series)
        """
        hour = data["tpep_pickup_datetime"].dt.hour
        return hour

    @staticmethod
    def _get_ride_duration_mins(data: pd.DataFrame) -> pd.Series:
        """
        Calculate ride duration from pick-up and drop-off times
        :param data: Input Data
        :return: Ride duration in minutes (Data Series)
        """
        duration = (data["tpep_dropoff_datetime"] - data["tpep_pickup_datetime"]) // pd.Timedelta(minutes=1)
        return duration

    def _fit_transform_one_hot_categorical(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Fit transform One hot categorical features
        :param data: Input dataframe
        :return:
        """
        features_OH_encoded = get_features_from_attribute(attribute="encodemethod",
                                                          value="OneHot")

        self._encoder = self._encoder.set_params(**{"handle_unknown": "ignore",
                                                    "sparse": True})
        oh_features = self._encoder.fit_transform(data[features_OH_encoded]).toarray()
        oh_feature_names = self._encoder.get_feature_names_out()

        oh_feature_df = pd.DataFrame(oh_features, columns=oh_feature_names, index=data.index)
        data = pd.concat([data, oh_feature_df], axis="columns")
        data = data.drop(columns=features_OH_encoded)

        self._logger.debug(f"Columns: {features_OH_encoded} One hot encoded")
        return data

    def _transform_one_hot_categorical(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Transform One hot categorical features
        :param data:
        :return:
        """
        features_OH_encoded = get_features_from_attribute(attribute="encodemethod",
                                                          value="OneHot")
        oh_features = self._encoder.transform(data[features_OH_encoded]).toarray()

        oh_feature_names = self._encoder.get_feature_names_out()

        oh_feature_df = pd.DataFrame(oh_features, columns=oh_feature_names, index=data.index)
        data = pd.concat([data, oh_feature_df], axis="columns")
        data = data.drop(columns=features_OH_encoded)

        self._logger.debug(f"Columns: {features_OH_encoded} One hot encoded")
        return data

    def _impute_nans(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Impute NaNs based on rules defines in Feature Attributes
        :param data: Input Data
        :return: Data with NaNs imputed
        """
        impute_features = get_features_from_attribute(attribute="impute_nan",
                                                      value=True)
        for feature in impute_features:
            value = self._feature_attributes[feature]["impute_nan_with"]
            data[feature] = data[feature].fillna(value)
            self._logger.debug(f"Imputed NaNs in Feature: {feature} with {value}")
        return data

    def load(self, data: pd.DataFrame, output_path: str) -> None:
        """
        Save data to output file
        :param data: Input DataFrame
        :param output_path: Output file path
        :return:
        """
        save_data(data, output_path)

    def save_processor(self) -> None:
        """
        Pickle feature processor encoder
        :return:
        """
        if not os.path.exists(self._save_path):
            os.makedirs(self._save_path)

        with open(f"{self._save_path}{os.sep}{__class__.__name__}.pkl", "wb") as f:
            pickle.dump(self, f)

        self._logger.info(f"Saved encoder to {self._save_path}{os.sep}{__class__.__name__}.pkl")

    def load_processor(self) -> None:
        """
        Load feature processor from file
        :return:
        """
        with open(f"{self._save_path}{os.sep}{__class__.__name__}.pkl", "rb") as f:
            processor = pickle.load(f)
        self._encoder = processor._encoder
        self._feature_attributes = processor._feature_attributes

        self._logger.info(f"Loaded encoder from {self._save_path}{os.sep}{__class__.__name__}.pkl")
