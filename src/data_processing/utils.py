import logging
import os.path
from typing import Tuple, List, Any

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split

from src.constants import FEATURE_ATTRIBUTES, TARGET, RANDOM_SEED, TEST_SET_SIZE
from src.logging_utils import init_logger

logger = logging.getLogger(__name__)
init_logger(logger)
np.random.seed(RANDOM_SEED)


def read_data(path: str) -> pd.DataFrame:
    """
    Read data from file
    :param path: Path to the data file
    :return: DataFrame
    """
    extension = path.split(".")[-1]
    if extension == "parquet":
        data = pd.read_parquet(path)
    elif extension == "csv":
        data = pd.read_csv(path, index_col=0)
    else:
        logger.error(f"Extension {extension} not supported")
        raise NotImplementedError()

    data.columns = data.columns.str.lower()
    logger.info(f"Loaded {data.shape[0]} rows and {data.shape[1]} columns")
    return data


def split_features_and_targets(data: pd.DataFrame) -> Tuple[pd.DataFrame, pd.Series]:
    """
    Split input data into features and targets
    :param data: Input Data
    :return: Feature DataFrame and Target Series
    """
    feature_columns = get_features_from_attribute(attribute="drop", value=False)
    features = data[feature_columns]
    target = data[TARGET]
    logger.info(f"Splitted data into {features.shape[1]} Features and Target")
    return features, target


def split_train_test(features: pd.DataFrame,
                     target: pd.Series,
                     split_size: int = TEST_SET_SIZE) -> \
        Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
    """
    Wrapper method to perform train test split
    :param features: Full Features DataFrame
    :param target: Full Target Series
    :param split_size: Test set size
    :return:
    """
    features_train, features_test, \
    target_train, target_test = train_test_split(
        features, target,
        test_size=split_size, random_state=RANDOM_SEED)
    logger.info(f"Splitted Data into Train Test Sets\n"
                f"Train Set Size, Features: {features_train.shape} Targets: {target_train.shape} \n"
                f"Test Set Size: Features: {features_test.shape} Targets: {target_test.shape}")
    return features_train, features_test, target_train, target_test


def save_data(data: pd.DataFrame, path: str) -> None:
    """
    Save data to parquet file
    :param data: DataFrame
    :param path: Output path
    :return: None
    """
    dir_path = f"{os.sep}".join(path.split(os.sep)[:-1])
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
        logger.info(f"Created Folder {dir_path}")

    extension = path.split(".")[-1]
    if extension == "parquet":
        data.to_parquet(path)
    elif extension == "csv":
        data.to_csv(path)
    else:
        logger.error(f"Extension {extension} not supported")
        raise NotImplementedError()

    logger.info(f"Saved data of size {data.shape} to {path}")


def get_features_from_attribute(attribute: str, value: Any) -> List:
    """
    Get list of features for a attribute: value pair
    Example:
        List all features with attribute = type, value = Categorical
    :param attribute: Attribute of a feature
    :param value: Value of the attribute
    :return: List of features
    """
    feature_list = []
    for feature, attributes in FEATURE_ATTRIBUTES.items():
        for attr, val in attributes.items():
            if attr == attribute and val == value:
                feature_list.append(feature)

    return feature_list
