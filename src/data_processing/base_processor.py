from abc import ABC, abstractmethod

import pandas as pd


class BaseDataProcessor(ABC):
    """
    Blueprint for Data Processor
    """
    def __init__(self):
        ...

    @abstractmethod
    def extract(self, **kwargs) -> pd.DataFrame:
        """
        Abstract method to extract data from a data source defined by args, kwargs
        :param kwargs: Custom parameters
        :return: DataFrame
        """
        ...

    @abstractmethod
    def transform(self, **kwargs) -> pd.DataFrame:
        """
        Abstract method to transform data by casting to correct data types,
        calculating additional features, encoding features, etc.
        :param kwargs: Custom parameters
        :return: Tuple of DataFrame
        """
        ...

    @abstractmethod
    def load(self, **kwargs) -> None:
        """
        Abstract method to load output data into filesystem or table depending on args, kwargs
        :param kwargs: Custom parameters
        :return: None
        """
        ...

    @abstractmethod
    def fit_transform(self, **kwargs) -> pd.DataFrame:
        """
        Abstract method to first fit encoders or imputers and then transform data
        :param kwargs:
        :return:
        """
        ...

    @abstractmethod
    def load_processor(self, **kwargs):
        """
        Abstract method to load fitted Feature Processor
        :param kwargs:
        :return:
        """
        ...
