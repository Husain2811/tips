"""
Command line interface for training model and serving predictions on datasets
"""

import src.boot
import fire

from src.runner import train, serve_test


def train_cli() -> None:
    """
    Cli interface for training
    """
    fire.Fire(train)


def serve_cli() -> None:
    """
    CLI interface for serving predictions on unknown datasets
    """
    fire.Fire(serve_test)

