import logging
from typing import Dict, Union

import pandas as pd

from src.data_processing.factory import get_feature_processor
from src.models.factory import get_model
from src.constants import (
    MODELS_PATH,
    SERVING_MODE,
    MODEL_HYPERPARAMETERS
)

from src.logging_utils import init_logger


class ServingPipeline:
    """
    Pipeline used to serve predictions
    1. Extracts data from a source depending on the mode of serving
    2. Transform features as per the models requirements
    3. Make Predictions
    """
    def __init__(self,
                 model_name: str,
                 processor_name: str,
                 verbosity: int = 0,
                 mode: int = 1):
        """
        Initialize serving pipeline
        :param model_name: Name of the model to serve with
        :param processor_name: Name of the Feature Processor
        :param verbosity: Logging verbosity
        :param mode: Serving mode. 0. Online, 1: Test set
        """
        self._logger = logging.getLogger(__class__.__name__)
        init_logger(self._logger, verbosity)

        self.feature_processor = get_feature_processor(
            processor_name=processor_name,
            encoder_path=MODELS_PATH,
            verbosity=verbosity)
        self.feature_processor.load_processor()

        self.model_hyperparameters = MODEL_HYPERPARAMETERS[model_name]

        self.model = get_model(
            model_name=model_name,
            hparams=self.model_hyperparameters,
            verbosity=verbosity)
        self.model.load()

        self.mode = SERVING_MODE[mode]

    def extract(self, source: Union[Dict, str]) -> pd.DataFrame:
        """
        Extract features from the data source
        :param source: Data source. Can be raw data in case of Online Mode or a file path in case of Test mode
        :return: Processed features DataFrame
        """
        if self.mode == "Online":
            if type(source) != dict:
                self._logger.info(f"Expected source type Dict, Got {type(source)}")
                raise AssertionError(f"Expected source type Dict, Got {type(source)}")
            features = self.feature_processor.extract(method="json", json=source)
            return features
        elif self.mode == "Test":
            if type(source) != str:
                self._logger.info(f"Expected str (path to a file), Got {type(source)}")
                raise AssertionError(f"Expected str (path to a file), Got {type(source)}")
            features = self.feature_processor.extract(method="file", path=source)
            return features
        else:
            self._logger.error("Invalid Serving Mode")
            raise NotImplementedError

    def run(self, data_source: Union[Dict, str]) -> Union[float, pd.Series]:
        """
        Run the pipeline
        :param data_source: Data source. Can be raw data in case of Online Mode or a file path in case of Test mode
        :return: Predictions. Can be a single value for Online mode or pandas Series for Test mode
        """
        features = self.extract(data_source)
        transformed_features = self.feature_processor.transform(data=features)
        predictions = self.model.predict(transformed_features,
                                         runtime_params={"n_jobs": -1})
        if self.mode == "Online":
            return predictions[0]
        else:
            predictions = pd.Series(predictions, index=features.index, name="predictions")
            return predictions
