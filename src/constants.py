import os

APP_NAME = "tip_predictor"
MODULE_PATH = os.path.dirname(__file__)
PROJECT_PATH = os.path.dirname(MODULE_PATH)

# Using os.sep as path separators because this app is not containerized and hence not OS agnostic
DATA_PATH = f"{PROJECT_PATH}{os.sep}data{os.sep}files"
DATASETS_PATH = f"{os.path.dirname(DATA_PATH)}{os.sep}datasets"
PROCESSED_DATASET_PATH = f"{os.path.dirname(DATA_PATH)}{os.sep}processed_datasets"
DATASET_NAME = "yellow_tripdata"

FEATURE_ATTRIBUTES = {
    "vendorid": {
        "type": "Categorical",
        "encodemethod": None,
        "drop": True,           # Does not seem important from description
    },
    "tpep_pickup_datetime": {
        "type": "Datetime",
        "drop": False,
    },
    "tpep_dropoff_datetime": {
        "type": "Datetime",
        "drop": False,
    },
    "passenger_count": {
        "type": "Numerical",
        "impute_nan": True,
        "impute_nan_with": 1,
        "drop": False,
    },
    "trip_distance": {
        "type": "Numerical",
        "drop": False,
    },
    "ratecodeid": {
        "type": "Categorical",
        "encodemethod": None,
        "drop": True,           # Does not seem important from description
    },
    "pulocationid": {
        "type": "Categorical",
        "encodemethod": None,
        "drop": True,
    },
    "dolocationid": {
        "type": "Categorical",
        "encodemethod": None,
        "drop": True,
    },
    "payment_type": {
        "type": "Categorical",
        "encodemethod": "OneHot",
        "drop": False,
    },
    "fare_amount": {
        "type": "Numerical",
        "drop": False,
    },
    "extra": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "mta_tax": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "tolls_amount": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "improvement_surcharge": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "congestion_surcharge": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "airport_fee": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    },
    "store_and_fwd_flag": {
        "type": "Categorical",
        "encodemethod": None,
        "drop": True,           # Does not seem important from description
    },
    "total_amount": {
        "type": "Numerical",
        "drop": True,           # Does not seem important from description
    }
}

TARGET = "tip_amount"

MODELS_PATH = f"{PROJECT_PATH}{os.sep}trained_models"
TEST_SET_SIZE = 10000

SERVING_MODE = {
    0: "Online",
    1: "Test",
}

RANDOM_SEED = 42

MODEL_HYPERPARAMETERS = {
    "RandomForestModel": {
        "n_estimators": 100,
        "criterion": "squared_error",
        "max_depth": None,
        "min_samples_split": 50,
        "min_samples_leaf": 20,
        "max_features": 0.6,
        "random_state": RANDOM_SEED,
        "ccp_alpha": 1.,
        "max_samples": 0.7,
        "bootstrap": True,
        "n_jobs": -1,
    },
    "ElasticNetModel": {
        "alpha": 2.0,
        "l1_ratio": 0.8,
        "max_iter": 10000,
        "random_state": RANDOM_SEED,
    }
}
