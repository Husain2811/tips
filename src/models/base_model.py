from typing import List
from abc import ABC, abstractmethod

import pandas as pd


class BaseModel(ABC):
    """
    Blueprint for Model
    """
    def __init__(self):
        ...

    @abstractmethod
    def fit(self, X: pd.DataFrame, y: pd.Series) -> None:
        """
        Abstract method to fit model on data and additional parameters defined by args, kwargs
        :param X: Feature DataFrame
        :param y: Target Series
        :return: DataFrame
        """
        ...

    @abstractmethod
    def predict(self, X: pd.DataFrame, **kwargs) -> List:
        """
        Abstract method to predict using fitted model
        :param X: Feature DataFrame
        :param kwargs: Additional params
        :return: Prediction Results as a list
        """
        ...

    @abstractmethod
    def load(self, **kwargs) -> None:
        """
        Abstract method to load model from path
        :param kwargs: Custom arguments
        :return: Model
        """
        ...

    @abstractmethod
    def save(self, **kwargs) -> None:
        """
        Abstract method to save model to path
        :param kwargs: Custom arguments
        :return: None
        """
        ...
