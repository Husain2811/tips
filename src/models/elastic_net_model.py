import os
import logging
from typing import Dict, List
import pickle

import pandas as pd
from sklearn.linear_model import ElasticNet

from src.logging_utils import init_logger
from src.constants import MODELS_PATH
from src.models.base_model import BaseModel


class ElasticNetModel(BaseModel):
    def __init__(self, hparams: Dict, verbosity: int = 0):
        """
        Initialize ElasticNet Model
        :param hparams: Hyperparameters
        :param verbosity: Logging verbosity
        """
        super().__init__()
        self._logger = logging.getLogger(__class__.__name__)
        init_logger(self._logger, verbosity)

        self._hyper_params = hparams
        self._model = None
        self._model_path = f"{MODELS_PATH}{os.sep}{__class__.__name__}.pkl"

    def fit(self, X: pd.DataFrame, y: pd.Series) -> None:
        """
        Train model
        :param X: Training data Features
        :param y: Training target
        :return: None
        """
        self._model = ElasticNet(**self._hyper_params)
        self._model.fit(X, y.values.ravel())
        self._logger.info("Finished training the model.")

    def save(self) -> None:
        """
        Save Model
        :return: None
        """
        if not os.path.exists(MODELS_PATH):
            os.makedirs(MODELS_PATH)
        pickle.dump(self._model, open(self._model_path, "wb"))
        self._logger.info(f"Saved model to {self._model_path}")

    def predict(self, X: pd.DataFrame, runtime_params: Dict = None) -> List:
        """
        Make predictions
        :param X: Input features to predict on
        :param runtime_params: Not used
        :return: Predictions
        """
        predictions = self._model.predict(X)
        self._logger.info(f"Made predictions for {len(X)} {'row' if len(X) == 1 else 'rows'}")
        return predictions

    def load(self) -> None:
        """
        Load model
        :return: None
        """
        self._model = pickle.load(open(self._model_path, "rb"))
        self._logger.info(f"Loaded model from {self._model_path}")