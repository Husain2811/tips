from typing import Dict

from src.models.base_model import BaseModel
from src.models.random_forest_model import RandomForestModel
from src.models.elastic_net_model import ElasticNetModel


def get_model(model_name: str, hparams: Dict, verbosity: int = 0) -> BaseModel:
    """
    Return the Model selected by model name

    :param hparams: Hyper parameters for the model
    :param model_name: Name of the processor to use
    :param verbosity: int
    :return: DataProcessor
    :raises NotImplementedError: If not supported yet.
    """
    models = {
        f"{RandomForestModel.__name__}": RandomForestModel(hparams, verbosity),
        f"{ElasticNetModel.__name__}": ElasticNetModel(hparams, verbosity),
    }

    try:
        return models[model_name]
    except KeyError:
        raise ModuleNotFoundError(f"Module for {model_name} model is not implemented yet...")