from typing import List
from setuptools import setup, find_packages
from src.constants import APP_NAME


def read_requirements() -> List:
    with open("requirements.txt", "r") as f:
        requirements = f.read().splitlines()
    return requirements


setup(
    name=APP_NAME,
    version="0.0.1",
    description="App to train model for estimating Tip amounts for Taxi drivers and serve them to a mobile app.",
    include_package_data=True,
    packages=find_packages(exclude=["tests"]),
    install_requires=read_requirements(),
    entry_points={
        "console_scripts": [f"{APP_NAME}_train=src.main:train_cli",
                            f"{APP_NAME}_serve_test=src.main:serve_cli",
                            f"{APP_NAME}_serve_online=app.server:serve_prediction"]
    },
)
