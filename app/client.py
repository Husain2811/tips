"""
GRPC Client that sends features to the server and receives predictions as response
"""

import app.boot
import fire
import logging

import predict_pb2
import predict_pb2_grpc
import time
import grpc
from grpc._channel import _InactiveRpcError
import random

from src.logging_utils import init_logger
from src.constants import RANDOM_SEED
from app.utils import generate_random_data


random.seed(RANDOM_SEED)
logger = logging.getLogger(__name__)
init_logger(logger)


def run() -> None:
    """
    Runs a single user as a GRPC client
    :return: None
    """
    with grpc.insecure_channel("localhost:9999") as channel:
        # A gRPC stub is an interface to the gRPC service.
        # The client makes an RPC call through a channel
        stub = predict_pb2_grpc.PredictionServiceStub(channel)
        counter = 1
        diff = 0
        while True:
            features = generate_random_data()
            try:
                start = time.time()

                # Sends a proto request message and receives a response
                response = stub.predict(predict_pb2.Features(**features))
                driver_id = response.driver_id
                ride_id = response.ride_id
                prediction = response.value
                logger.info(f"Prediction for Driver {driver_id}, Ride {ride_id}"
                            f": ${round(prediction, 2)}")

                stop = time.time()
                diff += (stop - start)
                counter += 1
                if counter % 100 == 0:
                    logger.info(f"{counter} predictions received in {diff} seconds, "
                                f"{round(100/diff, 2)} Responses per second")
                    diff = 0
                    counter = 0
            except KeyboardInterrupt:
                logger.error("Key Board Interrupt")
                channel.unsubscribe(close)
                exit()
            # Client shuts down when server does not respond,
            # so log it as error to continue process and not raise an error
            except _InactiveRpcError:
                logger.error("Received no response. Server Inactive!")


def close(channel):
    channel.close()


if __name__ == "__main__":
    fire.Fire(run)
