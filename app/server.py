"""
GRPC Server to serve tip amount predictions using a trained model
"""

import app.boot
import fire
import time
import logging
import threading
from concurrent import futures  # Run a threadpool executor

import grpc
import app.predict_pb2 as predict_pb2
import app.predict_pb2_grpc as predict_pb2_grpc
from google.protobuf.json_format import MessageToDict

from src.logging_utils import init_logger
from src.serving_pipeline import ServingPipeline


class Listener(predict_pb2_grpc.PredictionServiceServicer):
    """
    Listener Class that receives feature as message payload and sends predictions back to client
    """
    def __init__(self, verbosity: int = 0):
        # Count number of messages Server sends in a certain timeframe
        self._counter = 0
        self._time = time.time()
        self._logger = logging.getLogger(__class__.__name__)
        self.verbosity = verbosity
        init_logger(self._logger, verbose=self.verbosity)
        self.serve_pipe = ServingPipeline(model_name="ElasticNetModel",
                                          processor_name="FeatureProcessor",
                                          verbosity=self.verbosity,
                                          mode=0)

    def predict(self, request, context) -> predict_pb2.Prediction:
        """
        Serve predictions
        :param request: Message payload received from server
        :param context: Default context object of Servicer
        :return:
        """
        self._counter += 1
        if self._counter > 100:
            time_diff = time.time() - self._time
            self._logger.info(f"100 requests in {time_diff} seconds, "
                              f"{round(100/time_diff, 2)} Requests per Second")
            self._time = time.time()
            self._counter = 0

        data = MessageToDict(request)
        features = data.copy()
        del features["driverId"]
        del features["rideId"]

        prediction = self.serve_pipe.run(features)

        # Sends prediction as response back to client
        return predict_pb2.Prediction(driver_id=data["driverId"],
                                      ride_id=data["rideId"],
                                      value=prediction)


def serve_prediction(verbosity: int = 0) -> None:
    """
    Serve predictions using localhost:9999
    :param verbosity: Logging verbosity
    :return: None
    """
    logger = logging.getLogger(__name__)
    init_logger(logger, verbose=verbosity)

    # Execute calls asynchronously using a pool of 8 threads
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    predict_pb2_grpc.add_PredictionServiceServicer_to_server(Listener(verbosity), server)
    server.add_insecure_port("[::]:9999")
    server.start()
    try:
        while True:
            logger.info(f"Server on: threads {threading.active_count()}")
            time.sleep(10)
    except KeyboardInterrupt:
        logger.error(f"Key Board Interrupt")
        server.stop(0)


if __name__ == "__main__":
    fire.Fire(serve_prediction)
