"""
Updates PATH so that modules from outside this directory can be imported
"""

import os
import sys

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(PROJECT_DIR)
