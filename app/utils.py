import random
from typing import Dict

from datetime import datetime, timedelta
from google.protobuf.timestamp_pb2 import Timestamp


def generate_random_data() -> Dict:
    """
    Random data generator as message payload
    :return: Data Dictionary
    """
    pickup_ts = Timestamp()
    pickup_ts.FromDatetime(datetime.now())

    dropoff_ts = Timestamp()
    dropoff_ts.FromDatetime(datetime.now() + timedelta(hours=random.randint(0, 3),
                                                       minutes=random.randint(0, 60)))

    features = {
        "driver_id": random.randint(1, 10),
        "ride_id": random.randint(1, 10),
        "tpep_pickup_datetime": pickup_ts,
        "tpep_dropoff_datetime": dropoff_ts,
        "passenger_count": random.randint(1, 5),
        "trip_distance": random.random() * 50,
        "payment_type": random.randint(1, 4),
        "fare_amount": random.random() * 100,
    }
    return features
